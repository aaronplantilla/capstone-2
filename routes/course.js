const express = require('express')
const router = express.Router()
const auth = require("../auth")
const CourseController = require('../controllers/course')

//get all active courses
// router.get('/', (req, res) => {
// 	CourseController.getAllActive().then(resultsFromFind => res.send(resultsFromFind))
// })

//get all courses
router.get('/', (req, res) => {
	CourseController.getAllCourses().then(resultsFromFindAll => res.send(resultsFromFindAll))
})

//get a single course
router.get('/:courseId', (req, res) => {
	let courseId = req.params.courseId
	CourseController.get({courseId}).then(resultFromFindById => res.send(resultFromFindById))
})

//get courses name
router.get('/', auth.verify, (req, res) => {
	let courseName = req.params.courseName
	CourseController.get().then(resultFromGetByName => res.send(resultFromGetByName))
})

//create a new course
router.post('/', auth.verify, (req, res) => {
	CourseController.add(req.body).then(resultFromAdd => res.send(resultFromAdd))
})

//update a course
router.put('/', auth.verify, (req, res) => {
	CourseController.update(req.body).then(resultFromUpdate => res.send(resultFromUpdate))
})

//archive (delete) a course
router.delete('/:courseId', auth.verify, (req, res) => {
	let courseId = req.params.courseId
	CourseController.archive({courseId}).then(resultFromArchive => res.send(resultFromArchive))
})

//activate a course
router.delete('/active/:courseId', auth.verify, (req, res) => {
	let courseId = req.params.courseId
	CourseController.activate({courseId}).then(resultFromActivate => res.send(resultFromActivate))
})

module.exports = router