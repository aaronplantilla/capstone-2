const Course = require('../models/course')

//view all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(resultsFromFind => resultsFromFind)
}

// view all courses
module.exports.getAllCourses = () => {
	return Course.find().then(resultsFromFindAll => resultsFromFindAll)
}

//create/register a course
module.exports.add = (params) => {
	let newCourse = new Course({
		name: params.name,
		description: params.description,
		price: params.price
	})

	return newCourse.save().then((course, err) => {
		return (err) ? false : true
	})
}

//get a specific course
module.exports.get = (params) => {
	return Course.findById(params.courseId).then(resultFromFindById => resultFromFindById)
}

//get specific courses by name
module.exports.getCourseName = () => {
	return Course.findByCourseName().then(resultFromGetByName => resultFromGetByName)
}

//modify course details
module.exports.update = (params) => {
	let updatedCourse = {
		name: params.name,
		description: params.description,
		price: params.price
	}

	return Course.findByIdAndUpdate(params.courseId, updatedCourse).then((course, err) => {
		return (err) ? false : true
	})
}

//archive a course - set isActive to false
module.exports.archive = (params) => {
	let updateActive = {
		isActive: false
	}

	return Course.findByIdAndUpdate(params.courseId, updateActive).then((course, err) => {
		return (err) ? false : true
	})
}

//activate a course - set isActive to true
module.exports.activate = (params) => {
	let updateInactive = {
		isActive: true
	}

	return Course.findByIdAndUpdate(params.courseId, updateInactive).then((course, err) => {
		return (err) ? false : true
	})
}
