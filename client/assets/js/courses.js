let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector('#adminButton')
let cardFooter;
let courseInfo;
let token = localStorage.getItem("token")
let courseIds = []

//fetch the courses from our API
fetch('http://localhost:3000/api/courses')
.then(res => res.json())
.then(data => {
	// console.log(data)
	let courseData;

	//if the number of courses fetched is less than 1, display no courses available.
	if(data.length < 1){
		courseData = "No courses available."
	}else{
		//else iterate the courses collection and display each course
		courseData = data.map(course => {
			// console.log(course._id)
			if(adminUser == "false" || !adminUser){
				//check if the user is not an admin
				//if not an admin, display the select course button
				courseInfo = `<div class="card-body">
				<img src="${course.img}" class="thumb">
				<h5 class="card-title">${course.name}</h5>
				<p class="card-text text-left">${course.description}</p>
				<p class="card-text text-right">₱ ${course.price}</p>
				</div>`
				cardFooter = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-danger text-white btn-block editButton">Select Course</a>`

				if(course.isActive === false){
					courseInfo = `<div class="card-body" style="opacity: 0.5;">
					<img src="${course.img}" class="thumb">
					<h5 class="card-title">Coming Soon!</h5>
					</div>`
					cardFooter = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-danger text-white btn-block editButton disabled">Coming Soon!</a>`
				}else{
					// console.log("test")
				}

			}else{
				//if user is an admin, display the edit course button
				courseInfo = `<div class="card-body">
				<img src="${course.img}" class="thumb">
				<h5 class="card-title">${course.name}</h5>
				<p class="card-text text-left">${course.description}</p>
				<p class="card-text text-right">₱ ${course.price}</p>
				</div>`
				cardFooter = `<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-danger text-white btn-block editButton">Edit</a>

				<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-danger text-white btn-block editButton">Delete</a>

				<a href="./actCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-danger text-white btn-block editButton" id="deax">Activate</a>`

				if(course.isActive === true){
					courseInfo = `<div class="card-body">
					<img src="${course.img}" class="thumb">
					<h5 class="card-title">${course.name}</h5>
					<p class="card-text text-left">${course.description}</p>
					<p class="card-text text-right">₱ ${course.price}</p>
					</div>`
					cardFooter = `<a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-danger text-white btn-block editButton">Edit</a>

					<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-danger text-white btn-block editButton">Delete</a>

					<a href="./deactCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-danger text-white btn-block editButton" id="ax">Deactivate</a>`

				}else{
					// console.log("test")
				}
			}

			return (
				`<div class="col-md-6 my-3">
				<div class="card">
				${courseInfo}

				<div class="card-footer">
				${cardFooter}
				</div>
				</div>
				</div>`
				)

		}).join("")
		//since the collection is an array, we can use the join method to indicate the separator of each element. We replaced the commas with an empty strings to remove them.
	}



	let container = document.querySelector('#coursesContainer')
	container.innerHTML = courseData
})